package com.example.demo.repository;

import com.example.demo.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.ws.rs.PathParam;

@Repository
public interface IClient extends JpaRepository<Client, Long> {

    @Query("select c from Client c where c.email= :email and c.password = :password")
    Client login(@Param("email") String email, @Param("password") String password);


}