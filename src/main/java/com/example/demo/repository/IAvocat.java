package com.example.demo.repository;

import com.example.demo.entity.Avocat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAvocat extends JpaRepository<Avocat,Long> {
}
