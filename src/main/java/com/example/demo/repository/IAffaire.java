package com.example.demo.repository;

import com.example.demo.entity.Affaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.ws.rs.PathParam;

@Repository
public interface IAffaire extends JpaRepository<Affaire, Long> {


}