package com.example.demo.repository;

import com.example.demo.entity.Collegue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.ws.rs.PathParam;

@Repository
public interface ICollegue extends JpaRepository<Collegue, Long> {


}