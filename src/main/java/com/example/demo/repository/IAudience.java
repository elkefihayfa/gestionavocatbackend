package com.example.demo.repository;

import com.example.demo.entity.Audience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAudience extends JpaRepository<Audience,Long> {
}
