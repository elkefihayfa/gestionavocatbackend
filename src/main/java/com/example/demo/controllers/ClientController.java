package com.example.demo.controllers;

import com.example.demo.entity.Client;
import com.example.demo.repository.IClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    private IClient iClient;


    @GetMapping("/")
    public String homme() {
        return "okkkkkkkkk";
    }


    @GetMapping("/ById")
    public Client getClientById(@PathVariable Long id) {
        return iClient.getOne(id);
    }

    @GetMapping("/all")
    public List<Client> getALL() {
        return iClient.findAll();

    }


    @PostMapping("/add")
    public Client addClient(Client u) {

        return iClient.saveAndFlush(u);
    }

    @PutMapping("/update")
    public Client updateClient(Client u, @RequestParam Long id) {
        u.setId(id);
        return iClient.saveAndFlush(u);

    }


    @DeleteMapping("/delete")

    public String deleteClient(Long id) {
        try {
            iClient.deleteById(id);
            return "okkkkk";
        } catch (Exception e) {
            return "noooooooooooo";
        }
    }

    @PostMapping("/login")
    public Client loginClient(String email ,String password) {
        try {
           Client c= iClient.login(email,password);
            return c;
        } catch (Exception e) {
            return null;
        }
    }


}
