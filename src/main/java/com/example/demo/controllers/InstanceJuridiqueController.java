package com.example.demo.controllers;
import com.example.demo.entity.InstanceJuridique;
import com.example.demo.repository.IInstanceJuridique;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;





@RestController
@CrossOrigin("*")
@RequestMapping("/instanceJuridiques")


public class InstanceJuridiqueController {




    @Autowired
    private IInstanceJuridique iInstanceJuridique;






    @GetMapping("/all")
    public HashMap<String, List<InstanceJuridique>> getAllInstanceJuridique() {


        HashMap<String, List<InstanceJuridique>> hashMap = new HashMap<>();

        hashMap.put("data", iInstanceJuridique.findAll());

        return hashMap;
    }

    @PostMapping("/add")
    public HashMap<String, InstanceJuridique> addInstanceJuridique(InstanceJuridique u) {


        HashMap<String, InstanceJuridique> hashMap = new HashMap<>();


        hashMap.put("InstanceJuridique", iInstanceJuridique.save(u));

        return hashMap;
    }

    @PutMapping("/update/{id}")
    public HashMap<String, InstanceJuridique> update(InstanceJuridique u,@PathVariable Long id) {


        HashMap<String, InstanceJuridique> hashMap = new HashMap<>();
        u.setId(id);

        hashMap.put("InstanceJuridique", iInstanceJuridique.saveAndFlush(u));

        return hashMap;
    }


    @DeleteMapping("/delete/{id}")
    public HashMap<String, String> deleteInstanceJuridique(@PathVariable Long Id) {


        HashMap<String, String> hashMap = new HashMap<>();

        try {


            System.out.println(Id);
            iInstanceJuridique.deleteById(Id);
            hashMap.put("status", "InstanceJuridique BIEN SUPPRIMER");


        } catch (Exception e) {
            hashMap.put("status", "InstanceJuridique NON SUPPRIMER");
            hashMap.put("msg", e.getMessage());

        }

        return hashMap;
    }
    @GetMapping("/one/{id}")
    public HashMap<String, InstanceJuridique> getOneInstancejuridique(@PathVariable Long id) {


        HashMap<String, InstanceJuridique> hashMap = new HashMap<>();

        hashMap.put("InstanceJuridique", iInstanceJuridique.getOne(id));

        return hashMap;
    }

}
