package com.example.demo.controllers;
import com.example.demo.entity.Affaire;
import com.example.demo.repository.IAffaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;



@RestController
@CrossOrigin("*")
@RequestMapping("/affaires")
public class AffaireController {

    @Autowired
    private IAffaire iAffaire;



    @GetMapping("/all")
    public HashMap<String, List<Affaire>> getAllAffaire() {


        HashMap<String, List<Affaire>> hashMap = new HashMap<>();

        hashMap.put("listAffaire", iAffaire.findAll());

        return hashMap;
    }

    @PostMapping("/add")
    public HashMap<String, Affaire> addAffaire(Affaire u) {


        try {
            HashMap<String, Affaire> hashMap = new HashMap<>();


            hashMap.put("Affaire", iAffaire.save(u));
            return hashMap;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

        /* this is whatever code you have already...this is just an example */

    }

    @PutMapping("/update/{id}")
    public HashMap<String, Affaire> update(Affaire u,@PathVariable Long id) {


        HashMap<String, Affaire> hashMap = new HashMap<>();
        u.setId(id);

        hashMap.put("Affaire", iAffaire.saveAndFlush(u));

        return hashMap;


    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deleteAffaire(@PathVariable Long Id) {


        HashMap<String, String> hashMap = new HashMap<>();

        try {


            System.out.println(Id);
            iAffaire.deleteById(Id);
            hashMap.put("status", "Affaire BIEN SUPPRIMER");


        } catch (Exception e) {
            hashMap.put("status", "Affaire non SUPPRIMER");
            hashMap.put("message erreur", e.getMessage());

        }

        return hashMap;
    }
    @GetMapping("/one/{id}")
    public HashMap<String, Affaire> getAffaire(@PathVariable Long id) {


        HashMap<String, Affaire> hashMap = new HashMap<>();

        hashMap.put("Affaire", iAffaire.getOne(id));

        return hashMap;
    }

}
