package com.example.demo.controllers;

import com.example.demo.entity.Avocat;
import com.example.demo.repository.IAvocat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/avocats")
public class AvocatController {

    @Autowired
    private IAvocat iAvocat;


    @GetMapping("/all")
    public HashMap<String, List<Avocat>> getAllAvocat() {


        HashMap<String, List<Avocat>> hashMap = new HashMap<>();

        hashMap.put("listAvocat", iAvocat.findAll());

        return hashMap;
    }

    @PostMapping("/add")
    public HashMap<String, Avocat> addAvocat( Avocat u) {

        try {
            HashMap<String, Avocat> hashMap = new HashMap<>();


            hashMap.put("Avocat", iAvocat.save(u));

            return hashMap;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    @PutMapping("/update/{id}")
    public HashMap<String, Avocat> update( Avocat u, @PathVariable Long id) {


        HashMap<String, Avocat> hashMap = new HashMap<>();
        u.setId(id);

        hashMap.put("Avocat", iAvocat.saveAndFlush(u));

        return hashMap;
    }


    @DeleteMapping("/delete/{id}")
    public HashMap<String, String> deleteAvocat(Long Id, @PathVariable Long id) {


        HashMap<String, String> hashMap = new HashMap<>();

        try {


            System.out.println(Id);
            iAvocat.deleteById(Id);
            hashMap.put("status", "Avocat supprimee");


        } catch (Exception e) {
            hashMap.put("status", "Avocat non supprimee");
            hashMap.put("msg", e.getMessage());

        }

        return hashMap;
    }

    @GetMapping("/one/{id}")
    public HashMap<String, Avocat> getAvocat(@PathVariable Long id) {


        HashMap<String, Avocat> hashMap = new HashMap<>();

        hashMap.put("client", iAvocat.getOne(id));

        return hashMap;
    }


}
