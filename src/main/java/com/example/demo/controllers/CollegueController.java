package com.example.demo.controllers;
import com.example.demo.entity.Collegue;
import com.example.demo.repository.ICollegue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;



@RestController
@CrossOrigin("*")
@RequestMapping("/collegues")


public class CollegueController {




    @Autowired
    private ICollegue iCollegue;



    @GetMapping("/all")
    public HashMap<String, List<Collegue>> getAllCollegue() {


        HashMap<String, List<Collegue>> hashMap = new HashMap<>();

        hashMap.put("listcolleg", iCollegue.findAll());

        return hashMap;
    }

    @PostMapping("/add")
    public HashMap<String, Collegue> addCollegue(  Collegue u) {


        HashMap<String, Collegue> hashMap = new HashMap<>();


        hashMap.put("Collegue", iCollegue.save(u));

        return hashMap;
    }

    @PutMapping("/update/{id}")
    public HashMap<String, Collegue> update(Collegue u,@PathVariable Long id) {


        HashMap<String, Collegue> hashMap = new HashMap<>();
        u.setId(id);

        hashMap.put("Collegue", iCollegue.saveAndFlush(u));

        return hashMap;
    }


    @DeleteMapping("/delete")
    public HashMap<String, String> deleteCollegue(Long Id) {


        HashMap<String, String> hashMap = new HashMap<>();

        try {


            System.out.println(Id);
            iCollegue.deleteById(Id);
            hashMap.put("status", "collegue supprimer");


        } catch (Exception e) {
            hashMap.put("status", "collegue non supprimer");
            hashMap.put("msg", e.getMessage());

        }

        return hashMap;
    }
    @GetMapping("/one/{id}")
    public HashMap<String, Collegue> getOneCollegue(@PathVariable Long id) {


        HashMap<String, Collegue> hashMap = new HashMap<>();

        hashMap.put("Affaire", iCollegue.getOne(id));

        return hashMap;
    }


}
