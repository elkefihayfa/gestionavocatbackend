package com.example.demo.controllers;

import com.example.demo.entity.Audience;
import com.example.demo.repository.IAudience;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/audiences")
public class AudienceController {
    @Autowired
    private IAudience iAudience;

    @GetMapping("/all")
    public HashMap<String, List<Audience>> getAllAudience() {


        HashMap<String, List<Audience>> hashMap = new HashMap<>();

        hashMap.put("listAudience", iAudience.findAll());

        return hashMap;
    }

    @PostMapping("/add")
    public HashMap<String, Audience> addAudience(@RequestBody Audience u) {


        HashMap<String, Audience> hashMap = new HashMap<>();


        hashMap.put("Affaire", iAudience.save(u));

        return hashMap;
    }

    @PutMapping("/update/{id}")
    public HashMap<String, Audience> update(Audience u,@PathVariable Long id) {


        HashMap<String, Audience> hashMap = new HashMap<>();
        u.setId(id);

        hashMap.put("Affaire", iAudience.saveAndFlush(u));

        return hashMap;
    }


    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deleteAudience(@PathVariable Long Id) {


        HashMap<String, String> hashMap = new HashMap<>();

        try {


            System.out.println(Id);
            iAudience.deleteById(Id);
            hashMap.put("status", "Audience BIEN SUPPRIMER");


        } catch (Exception e) {
            hashMap.put("status", "Audience non SUPPRIMER");
            hashMap.put("message erreur", e.getMessage());

        }

        return hashMap;
    }
    @GetMapping("/one/{id}")
    public HashMap<String, Audience> getAudience(@PathVariable Long id) {


        HashMap<String, Audience> hashMap = new HashMap<>();

        hashMap.put("Audience", iAudience.getOne(id));

        return hashMap;
    }

}
