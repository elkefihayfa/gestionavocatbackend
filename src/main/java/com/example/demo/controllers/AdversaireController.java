package com.example.demo.controllers;
import com.example.demo.entity.Adversaire;
import com.example.demo.repository.IAdversaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;



@RestController
@CrossOrigin("*")
@RequestMapping("/adversaires")




public class AdversaireController {

	@Autowired
	private IAdversaire iAdversaire;



	@GetMapping("/all")
	public HashMap<String, List<Adversaire>> getAllAdversaire() {


		HashMap<String, List<Adversaire>> hashMap = new HashMap<>();

		hashMap.put("listAdversaire", iAdversaire.findAll());

		return hashMap;
	}

	@PostMapping("/add")
	public HashMap<String, Adversaire> addAdversaire(Adversaire u) {


		try {
			HashMap<String, Adversaire> hashMap = new HashMap<>();


			hashMap.put("Adversaire", iAdversaire.save(u));
			return hashMap;
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}


		/* this is whatever code you have already...this is just an example */


		}

	@PutMapping("/update/{id}")
	public HashMap<String, Adversaire> update(  Adversaire u,@PathVariable Long id) {


		HashMap<String, Adversaire> hashMap = new HashMap<>();
		u.setId(id);

		hashMap.put("Adversaire", iAdversaire.saveAndFlush(u));

		return hashMap;





	}


	@DeleteMapping("/delete/{Id}")
	public HashMap<String, String> deleteAdversaire(@PathVariable Long Id) {


		HashMap<String, String> hashMap = new HashMap<>();

		try {


			System.out.println(Id);
			iAdversaire.deleteById(Id);
			hashMap.put("status", "Adversaire BIEN SUPPRIMER");


		} catch (Exception e) {
			hashMap.put("status", "Adversaire non SUPPRIMER");
			hashMap.put("message erreur", e.getMessage());

		}

		return hashMap;
	}
	@GetMapping("/one/{id}")
	public HashMap<String, Adversaire> getAdversaire(@PathVariable Long id) {


		HashMap<String, Adversaire> hashMap = new HashMap<>();

		hashMap.put("Adversaire", iAdversaire.getOne(id));

		return hashMap;
	}

}
