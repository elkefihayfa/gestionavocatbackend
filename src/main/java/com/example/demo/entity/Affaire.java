package com.example.demo.entity;
import java.io.Serializable;
import java.util.Date;
import java.util.*;
import java.util.List;
import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonBackReference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name="Affaire")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Affaire implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long numAffaire;

    private Date dateEntree;

    private String statutClient;

    private String typeAdversaire;

    private String avocatAdversaire;

    private String natureAdversaire;

    private String frais;

    private String remarques;

    private String sujet;





/*--------------------------------------------------------------------------------------------------------------------*/
    @JsonIgnoreProperties(value = {"affaires"})
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="client_id", nullable=false)

    private Client client;
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
/*--------------------------------------------------------------------------------------------------------------------*/



    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="avocat_id", nullable=false)
    @JsonBackReference
    private Avocat avocat;
    public Avocat getAvocat() {
        return avocat;
    }
    public void setAvocat(Avocat avocat) {
        this.avocat = avocat;
    }
/*--------------------------------------------------------------------------------------------------------------------*/

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="adversaire_id", nullable=false)
    @JsonBackReference
    private Adversaire adversaire;
    public Adversaire getAdversaire() {
        return adversaire;
    }
    public void setAdversaire(Adversaire adversaire) {
        this.adversaire = adversaire;
    }
 /*--------------------------------------------------------------------------------------------------------------------*/
    @JsonIgnoreProperties(value = {"affaires"})
    @ManyToMany(mappedBy = "affaires", fetch = FetchType.LAZY)
    @JsonManagedReference
    private Set<Audience> audiences = new HashSet<>();
  /*--------------------------------------------------------------------------------------------------------------------*/




    public Affaire() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumAffaire() {
        return numAffaire;
    }

    public void setNumAffaire(Long numAffaire) {
        this.numAffaire = numAffaire;
    }

    public Date getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }
    public String getStatutClient() {
        return statutClient;
    }
    public void setStatutClient(String statutClient) {
        this.statutClient = statutClient;
    }
    public String getTypeAdversaire() {
        return typeAdversaire;
    }
    public void setTypeAdversaire(String typeAdversaire) {
        this.typeAdversaire = typeAdversaire;
    }
    public String getAvocatAdversaire() {
        return avocatAdversaire;
    }


    public void setAvocatAdversaire(String avocatAdversaire) {
        this.avocatAdversaire = avocatAdversaire;
    }


    public String getNatureAdversaire() {
        return natureAdversaire;
    }



    public void setNatureAdversaire(String natureAdversaire) {
        this.natureAdversaire = natureAdversaire;
    }





    public String getFrais() {
        return frais;
    }

    public void setFrais(String frais) {
        this.frais = frais;
    }

    public String getRemarques() {
        return remarques;
    }

    public void setRemarques(String remarques) {
        this.remarques = remarques;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }


    public Affaire(Long numAffaire, Date dateEntree, String statutClient, String typeAdversaire,   String natureAdversaire,  String frais, String remarques, String sujet,   Client client,   Avocat avocat,Adversaire adversaire) {

        this.numAffaire = numAffaire;
        this.dateEntree = dateEntree;
        this.statutClient = statutClient;
        this.typeAdversaire = typeAdversaire;
        this.natureAdversaire = natureAdversaire;
        this.frais = frais;
        this.remarques = remarques;
        this.sujet = sujet;
        this.client = client;
        this.avocat = avocat;
        this.adversaire=adversaire;
    }

    @Override
    public String toString() {
        return "Affaire [id=" + id + ", numAffaire=" + numAffaire + ", dateEntree=" + dateEntree + ", statutClient="
                + statutClient + ", typeAdversaire=" + typeAdversaire + ",  avocatAdversaire=" + avocatAdversaire + ", natureAdversaire=" + natureAdversaire + ", client=" + client + ", frais=" + frais + ", remarques="
                + remarques + ", sujet=" + sujet + "]";
    }




}