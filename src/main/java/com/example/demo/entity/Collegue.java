package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Collegue")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Collegue extends Personne {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String designation;

	/*--------------------------------------------------------------------------------------------------------------------*/
	@ManyToOne
	private Avocat avocat1;
	public Avocat getAvocat1() {
		return avocat1;
	}
	public void setAvocat1(Avocat avocat1) {
		this.avocat1 = avocat1;
	}
	/*--------------------------------------------------------------------------------------------------------------------*/




	public Collegue(String nom, String prenom, String adresse, String gouvernorat, Long fax, Long tel,  String designation) {

		super(nom, prenom, adresse, gouvernorat, fax, tel);
		this.designation = designation;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Collegue() {
	}


}