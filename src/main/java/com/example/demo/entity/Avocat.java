package com.example.demo.entity;

import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Avocat")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Avocat extends Personne {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;



	/*--------------------------------------------------------------------------------------------------------------------*/




	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="avocat")
	@JsonManagedReference
	private Set<Client> clients;
	public Set<Client> getClients() {
		return clients;
	}
	public void setClients(Set<Client> clients) {
		this.clients = clients;
	}



	/*--------------------------------------------------------------------------------------------------------------------*/


	@OneToMany
	private Set<Affaire> affaires = new HashSet<Affaire>();
/*--------------------------------------------------------------------------------------------------------------------*/

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Avocat(String nom, String prenom, String adresse, String gouvernorat, Long fax, Long tel) {
		super(nom, prenom, adresse, gouvernorat, fax, tel);
	}

	public Avocat() {
	}
}
