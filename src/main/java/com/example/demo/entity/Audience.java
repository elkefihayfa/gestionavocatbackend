package com.example.demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Audience implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String heurAffaire;
	private String sujetAffaire;
	private String jugementaffaire;


	/*--------------------------------------------------------------------------------------------------------------------*/
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "audience_affaires",
			joinColumns = {
					@JoinColumn(name = "audience_id", referencedColumnName = "id",
							nullable = false, updatable = false)},
			inverseJoinColumns = {
					@JoinColumn(name = "affaires_id", referencedColumnName = "id",
							nullable = false, updatable = false)})
	private Set<Affaire> affaires = new HashSet<>();
	/*--------------------------------------------------------------------------------------------------------------------*/

	@ManyToMany
	private List<InstanceJuridique> instanceJuridiques;
	public List<InstanceJuridique> getInstanceJuridiques() {
		return instanceJuridiques;
	}
	public void setInstanceJuridiques(List<InstanceJuridique> instanceJuridiques) {
		this.instanceJuridiques = instanceJuridiques;
	}
	/*--------------------------------------------------------------------------------------------------------------------*/





	/*--------------------------------------------------------------------------------------------------------------------*/


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHeurAffaire() {
		return heurAffaire;
	}

	public void setHeurAffaire(String heurAffaire) {
		this.heurAffaire = heurAffaire;
	}

	public String getSujetAffaire() {
		return sujetAffaire;
	}

	public void setSujetAffaire(String sujetAffaire) {
		this.sujetAffaire = sujetAffaire;
	}

	public String getJugementaffaire() {
		return jugementaffaire;
	}

	public void setJugementaffaire(String jugementaffaire) {
		this.jugementaffaire = jugementaffaire;
	}




	public Audience(String heurAffaire, String sujetAffaire, String jugementaffaire,
			List<InstanceJuridique> instanceJuridiques) {
		this.heurAffaire = heurAffaire;
		this.sujetAffaire = sujetAffaire;
		this.jugementaffaire = jugementaffaire;

		this.instanceJuridiques = instanceJuridiques;
	}

	public Audience() {
	}
}
