package com.example.demo.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Adversaire")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Adversaire extends Personne {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dateNais;
	private String lieuNaiss;
	private String cin;
	private String dossAdmini;
	private String nomAdmini;

	/*--------------------------------------------------------------------------------------------------------------------*/

	@JsonIgnoreProperties(value = {"adversaire"})
	@OneToMany(mappedBy="adversaire")

	private Set<Affaire> affaires;
	public Set<Affaire> getAffaires() {
		return affaires;
	}
	public void setAffaires(Set<Affaire> affaires) {
		this.affaires = affaires;
	}
	/*--------------------------------------------------------------------------------------------------------------------*/

	public Adversaire() {
		super();
	}



	public Adversaire(String nom, String prenom, String adresse, String gouvernorat, Long fax, Long tel,
			String dateNais, String lieuNaiss, String cin, String dossAdmini, String nomAdmini) {
		super(nom, prenom, adresse, gouvernorat, fax, tel);
		this.dateNais = dateNais;
		this.lieuNaiss = lieuNaiss;
		this.cin = cin;
		this.dossAdmini = dossAdmini;
		this.nomAdmini = nomAdmini;

	}

	public String getDateNais() {
		return dateNais;
	}

	public void setDateNais(String dateNais) {
		this.dateNais = dateNais;
	}

	public String getLieuNaiss() {
		return lieuNaiss;
	}

	public void setLieuNaiss(String lieuNaiss) {
		this.lieuNaiss = lieuNaiss;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getDossAdmini() {
		return dossAdmini;
	}

	public void setDossAdmini(String dossAdmini) {
		this.dossAdmini = dossAdmini;
	}

	public String getNomAdmini() {
		return nomAdmini;
	}

	public void setNomAdmini(String nomAdmini) {
		this.nomAdmini = nomAdmini;
	}

	@Override
	public String toString() {
		return "Adversaire [dateNais=" + dateNais + ", lieuNaiss=" + lieuNaiss + ", cin=" + cin + ", dossAdmini="
				+ dossAdmini + ", nomAdmini=" + nomAdmini + ", affaires=" + affaires + "]";
	}


}