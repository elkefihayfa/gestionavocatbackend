package com.example.demo.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Client")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String prenom;
	private String adresse;
	private String gouvernorat;
	private Long fax;
	private Long tel;
	private String typeClient;
	private String naiss;
	private String lieunaiss;
	private String municipalite;
	private String cin;
	private String authentifieDe;
	private Date Datecin;
	private Long tel2;
	private String email;
	private String password;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getGouvernorat() {
		return gouvernorat;
	}

	public void setGouvernorat(String gouvernorat) {
		this.gouvernorat = gouvernorat;
	}

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}

	public Long getTel() {
		return tel;
	}

	public void setTel(Long tel) {
		this.tel = tel;
	}


	/*--------------------------------------------------------------------------------------------------------------------*/
	@JsonIgnoreProperties(value = {"client"})
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "client")
	private Set<Affaire> affaires;
	public Set<Affaire> getAffaires() {
		return affaires;
	}
	public void setAffaires(Set<Affaire> affaires) {
		this.affaires = affaires;
	}

	/*--------------------------------------------------------------------------------------------------------------------*/

	@ManyToOne(cascade=CascadeType.ALL)

	@JoinColumn(name="avocat_id", nullable=false)
	@JsonBackReference
	private Avocat avocat;
	public Avocat getAvocat() {
		return avocat;
	}
	public void setAvocat(Avocat avocat) {
		this.avocat = avocat;
	}
/*--------------------------------------------------------------------------------------------------------------------*/

	public String getTypeClient() {
		return typeClient;
	}

	public void setTypeClient(String typeClient) {
		this.typeClient = typeClient;
	}

	public String getNaiss() {
		return naiss;
	}

	public void setNaiss(String naiss) {
		this.naiss = naiss;
	}

	public String getLieunaiss() {
		return lieunaiss;
	}

	public void setLieunaiss(String lieunaiss) {
		this.lieunaiss = lieunaiss;
	}

	public String getMunicipalite() {
		return municipalite;
	}

	public void setMunicipalite(String municipalite) {
		this.municipalite = municipalite;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getAuthentifieDe() {
		return authentifieDe;
	}

	public void setAuthentifieDe(String authentifieDe) {
		this.authentifieDe = authentifieDe;
	}

	public Date getDatecin() {
		return Datecin;
	}

	public void setDatecin(Date datecin) {
		Datecin = datecin;
	}

	public Long getTel2() {
		return tel2;
	}

	public void setTel2(Long tel2) {
		this.tel2 = tel2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
