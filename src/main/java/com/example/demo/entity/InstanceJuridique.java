package com.example.demo.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "InstanceJuridique")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class InstanceJuridique implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String region;
	private String gouvernorat;
	private String adresse;
	private Long tel;
	private Long tel2;
	private Long fax;
	private Long fax2;


/*--------------------------------------------------------------------------------------------------------------------*/
	@ManyToMany
	private List<Audience> audiences;
	public List<Audience> getAudiences() {
		return audiences;
	}
	public void setAudiences(List<Audience> audiences) {
		this.audiences = audiences;
	}
/*--------------------------------------------------------------------------------------------------------------------*/

	public InstanceJuridique() {
	}


	public InstanceJuridique(String region, String gouvernorat, String adresse, Long tel, Long tel2, Long fax,
			Long fax2, List<Audience> audiences) {
		this.region = region;
		this.gouvernorat = gouvernorat;
		this.adresse = adresse;
		this.tel = tel;
		this.tel2 = tel2;
		this.fax = fax;
		this.fax2 = fax2;
		this.audiences = audiences;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getGouvernorat() {
		return gouvernorat;
	}

	public void setGouvernorat(String gouvernorat) {
		this.gouvernorat = gouvernorat;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Long getTel() {
		return tel;
	}

	public void setTel(Long tel) {
		this.tel = tel;
	}

	public Long getTel2() {
		return tel2;
	}

	public void setTel2(Long tel2) {
		this.tel2 = tel2;
	}

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}

	public Long getFax2() {
		return fax2;
	}

	public void setFax2(Long fax2) {
		this.fax2 = fax2;
	}

}